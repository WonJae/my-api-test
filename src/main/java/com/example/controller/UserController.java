package com.example.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.example.jpa.SimpleCommentRepository;
import com.example.model.UserInfo;

@Controller
@RequestMapping("/")
public class UserController {
	
	@Autowired
	private SimpleCommentRepository simpleCommentRepository;
	
	@RequestMapping(method=RequestMethod.GET)
	public String viewGet(Model model) {
		
		List<UserInfo> userList = simpleCommentRepository.findAll();
		//model.addAttribute("CommentList", "");
		System.out.println(userList);
		return "demo";
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public String viewPost(Model model) {
		
		List<UserInfo> userList = simpleCommentRepository.findAll();
		//model.addAttribute("CommentList", "");
		System.out.println(userList);
		return "redirect:/";
	}
}
