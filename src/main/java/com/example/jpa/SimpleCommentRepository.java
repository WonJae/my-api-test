package com.example.jpa;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.model.UserInfo;

public interface SimpleCommentRepository extends JpaRepository<UserInfo, Long> {
	List<UserInfo> findAll();
}